// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAddDocument: null,
	fileProgressPercentage: 0,
	statusDocument: 'add',
	previewFiles: '0lmtBTpeBUgA7V1n2NY6.pdf'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region Document DISCLAIMER
	async getListAdmin ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchAdmin({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/document/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addAdmin ({ commit }, payload) {
		// console.log(this.state.auth.user)
		let formData = new FormData()
		formData.append('ref_number', payload.ref_number)
		formData.append('type_of_letter', payload.type_of_letter)
		formData.append('publish_date', payload.publish_date)
		formData.append('title', payload.title)
		formData.append('from', payload.from)
		formData.append('to', payload.to)
		formData.append('letter_loc_number', payload.letter_loc_number)
		formData.append('files', payload.files)
		return await this.$axios.post('cms/document/add', formData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editDAdmin({ commit }, payload) {
		return await this.$axios.post('cms/document/update', payload,
		{
			// headers: {
			// 	'Content-Type': 'multipart/form-data'
			// },
			onUploadProgress: function( progressEvent ) {
				let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
				this.commit('setting/filePercentage', uploadPercentage)
			}.bind(this)
		})
	.then((response) => {
			// console.log()
			return response.data
		})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
					return {
						success: false,
						message: err,
						info: 'some error'
					}
				}
			})
	},
	async editIsActiveAdmin({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/disaster/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteDAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/users/admin/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	setFilePreview (state, payload) {
		state.previewFiles = payload
	},
	clearData(state){
		state.tempImageFile = null
		state.dataAddDocument = null
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	updateStatusDocument(state, payload){
		state.statusDocument = payload
	},
	savedStateAddDocument(state, payload){
		// console.log(payload)
		state.dataAddDocument = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddDocument.image_url = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


