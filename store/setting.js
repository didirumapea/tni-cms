let urlCdn = process.env.CDN_URL;
if (process.env.NODE_ENV === 'development'){
	urlCdn = process.env.CDN_URL;
	// urlCdn = 'https://cdnx.bumikita.or.id/';
}

import moment from "moment";

// STATE AS VARIABLE
export const state = () => ({
	pathFilesDocument: `${urlCdn}assets/files/document/`,
    d2: null,
    timezone: moment().utcOffset()/60,
	fileProgressPercentage: 0,
	statusFileProgress: 'add',
})
// ACTIONS AS METHODS
export const actions = { // asyncronous

}
// MUTATION AS LOGIC
export const mutations = { // syncronous

}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}
