module.exports = {
	  apps : [
		{
		  name: "tni-cms-dev",
		  script: "npm",
		  args: "run dev"
		},
		{
		  name: "tni-cms-prod",
		  script: "npm",
		  args: "run start"
		},
		{
		  name: "tni-cms-staging",
		  script: "npm",
		  args: "run start"
		}
	  ]
}
